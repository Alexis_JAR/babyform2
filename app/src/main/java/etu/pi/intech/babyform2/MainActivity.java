package etu.pi.intech.babyform2;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    Button boutonBd;
    Button boutonVideos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boutonBd = findViewById(R.id.buttonAccesBd);
        boutonVideos = findViewById(R.id.buttonAccesVideo);

        boutonBd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent bdActivity = new Intent(MainActivity.this, BdList.class);
                startActivity(bdActivity);
            }
        });

        boutonVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*Intent videoListActivity = new Intent(MainActivity.this, VideoList.class);
                startActivity(videoListActivity);*/
                Intent videoActivity = new Intent(MainActivity.this, LecteurVideo.class);
                startActivity(videoActivity);
            }
        });
    }
}
