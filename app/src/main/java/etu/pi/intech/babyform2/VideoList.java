package etu.pi.intech.babyform2;

import android.content.res.AssetManager;
import android.os.Environment;
import android.text.Editable;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.io.IOException;

public class VideoList extends AppCompatActivity {
    String path = Environment.getExternalStorageDirectory().toString() + "/raw";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);



        populateListView();
    }

    private void populateListView() {
        //String[] videos = ;//trouver tout ce que contient le dossier des videos

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.video_items);

        ListView listVid = (ListView) findViewById(R.id.listViewVideos);
        listVid.setAdapter(adapter);
    }
}