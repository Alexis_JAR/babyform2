package etu.pi.intech.babyform2;

import android.net.Uri;
import android.widget.MediaController;
import android.widget.VideoView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;


public class LecteurVideo extends AppCompatActivity {

    VideoView video;

    MediaController mControl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lecteur_video);

        video = findViewById(R.id.pratiksExampleVideoView);

        mControl = new MediaController(this);

        video.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.que_faire_en_cas_de_brulure_premiers_secours));

        video.setMediaController(mControl);

        mControl.setAnchorView(video);

        video.start();
    }
    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
